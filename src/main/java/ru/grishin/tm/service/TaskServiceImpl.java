package ru.grishin.tm.service;

import ru.grishin.tm.api.TaskService;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.repository.TaskRepository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository = new TaskRepository();

    @Override
    public void createTask(String projectId, String name, String description, Date dateStart, Date dateFinish) {
        taskRepository.createTask(projectId, generateRandomBasedUUID(), name, description, dateStart, dateFinish);
    }

    @Override
    public void deleteTask(String id) {
        taskRepository.deleteTask(id);
    }

    @Override
    public void updateTask(String id, String name, String description, Date dateStart, Date dateFinish) {
        taskRepository.updateTask(id, name, description, dateStart,dateFinish);
    }

    public void deleteAllTaskByProjectId(String projectId){
        taskRepository.deleteAllTaskByProjectId(projectId);
    }

    @Override
    public Task readTask(String projectId) {
        return null;
    }

    @Override
    public List<Task> showAll() {
        return taskRepository.showTaskList();
    }

    public String generateRandomBasedUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
