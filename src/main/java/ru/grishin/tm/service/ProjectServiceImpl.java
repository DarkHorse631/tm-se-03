package ru.grishin.tm.service;

import ru.grishin.tm.api.ProjectService;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.repository.ProjectRepository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ProjectServiceImpl implements ProjectService {

    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskServiceImpl taskService = new TaskServiceImpl();

    @Override
    public void createProject(String name, String description, Date dateStart, Date dateFinish) {
        projectRepository.createProject(generateRandomBasedUUID(), name, description, dateStart, dateFinish);
    }

    @Override
    public void deleteProject(String id) {
        projectRepository.deleteProject(id);
    }

    @Override
    public void update(String id, String name, String description, Date dateStart, Date dateFinish) {
        projectRepository.update(id, name, description, dateStart, dateFinish);
    }

    @Override
    public Project readProject(String id) {
        return projectRepository.readProject(id);
    }

    public List<Project> showAll() {
        return projectRepository.showAll();
    }

    public String generateRandomBasedUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

}
