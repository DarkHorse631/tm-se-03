package ru.grishin.tm.ui;

import ru.grishin.tm.entity.Project;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.service.ProjectServiceImpl;
import ru.grishin.tm.service.TaskServiceImpl;

import java.util.Date;
import java.util.Scanner;

public class Menu {

    private ProjectServiceImpl projectService = new ProjectServiceImpl();
    private TaskServiceImpl taskService = new TaskServiceImpl();
    private Scanner scanner = new Scanner(System.in);
    private String projectId;
    private String taskId;
    private String name;
    private String description;


    public void createProject() {
        System.out.println("--Create project--");
        System.out.print("Enter name: ");
        name = scanner.nextLine();
        System.out.print("Enter description: ");
        description = scanner.nextLine();
        projectService.createProject(name, description, new Date(), new Date());
        System.out.println("[OK]");
    }

    public void showProjectList() {
        System.out.println("--Show all projects--");
        for (Project p : projectService.showAll()) {
            System.out.println(p);
        }
    }

    public void deleteProject() {
        System.out.println("--Remove project--");
        System.out.print("Enter the project id: ");
        projectId = scanner.nextLine();
        projectService.deleteProject(projectId);
        taskService.deleteAllTaskByProjectId(projectId);
        System.out.println("[PROJECT DELETED]");
    }

    public void updateProject() {
        System.out.println("--Update project--");
        System.out.println("Enter project id: ");
        projectId = scanner.nextLine();
        System.out.print("Enter new name: ");
        name = scanner.nextLine();
        System.out.print("Enter new description: ");
        description = scanner.nextLine();
        projectService.update(projectId, name, description, new Date(), new Date());
        System.out.println("[PROJECT UPDATED]");
    }

    public void clearProject() {
        System.out.println("--Clear Project--");
        System.out.print("Enter project id: ");
        projectId = scanner.nextLine();
        taskService.deleteAllTaskByProjectId(projectId);
        System.out.println("[PROJECT CLEAR]");
    }

    public void createTask() {
        System.out.println("--Create task into project--");
        System.out.print("Enter project id: ");
        projectId = scanner.nextLine();
        System.out.print("Enter task name: ");
        name = scanner.nextLine();
        System.out.print("Enter task description: ");
        description = scanner.nextLine();
        taskService.createTask(projectId, name, description, new Date(), new Date());
        System.out.println("[TASK CREATED]");
    }

    public void updateTask(){
        System.out.println("--Update task--");
        System.out.print("Enter task id:");
        taskId = scanner.nextLine();
        System.out.print("Enter new name:");
        name = scanner.nextLine();
        System.out.print("Enter new description:");
        description = scanner.nextLine();
        taskService.updateTask(taskId,name,description,new Date(), new Date());
        System.out.println("[TASK UPDATED]");
    }

    public void deleteTask() {
        System.out.println("--Remove task from project--");
        System.out.print("Enter task id: ");
        taskId = scanner.nextLine();
        taskService.deleteTask(taskId);
        System.out.println("[TASK DELETED]");

    }

    public void showTaskList() {
        System.out.println("--Task list--");
        for (Task t : taskService.showAll()) {
            System.out.println(t);
        }
    }

    public void exit(){
        System.exit(0);
    }

    public void showHelpList() {
        System.out.println("[help]: Show all commands. " +
                "\n [pc]: Create new project." +
                "\n [pu]: Update selected project." +
                "\n [psa]: Show all projects." +
                "\n [pr]: Remove selected project." +
                "\n [pcl]: Remove all tasks from project" +
                "\n" +
                "\n [tc]: Create new task." +
                "\n [tu]: Update selected task." +
                "\n [tsa]: Show all tasks." +
                "\n [tr]: Remove selected task." +
                "\n [tdpi]: Remove all task by project id." +
                "\n " +
                "\n [exit]: Exit from app.");
    }
}
