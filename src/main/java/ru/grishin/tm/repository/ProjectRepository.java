package ru.grishin.tm.repository;

import ru.grishin.tm.entity.Project;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProjectRepository {

    private List<Project> projectList = new ArrayList<>();

    public void createProject(String projectId, String projectName, String description, Date startDate, Date completionDate) {
        projectList.add(new Project(projectId, projectName, description, startDate, completionDate));
    }

    public Project readProject(String projectId) {
        for (Project project : projectList) {
            if (project.getId().equals(projectId))
                return project;
        }
        return null;
    }

    public void deleteProject(String projectId) {
        for (Project project : projectList) {
            if (project.getId().equals(projectId)) {
                projectList.remove(project);
                break;
            }
        }
    }

    public List<Project> showAll() {
        return projectList;
    }

    public void update(String projectId, String projectName, String description, Date startDate, Date completionDate) {
        for (Project project : projectList) {
            if (project.getId().equals(projectId)) {
                project.setName(projectName);
                project.setDescription(description);
                project.setDateStart(startDate);
                project.setDateFinish(completionDate);
            }
        }
    }

    public void clearProjectList() {
        projectList.removeAll(projectList);
    }

}
