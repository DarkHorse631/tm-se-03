package ru.grishin.tm.repository;

import ru.grishin.tm.entity.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskRepository {

    private List<Task> taskList = new ArrayList<>();
    private List<Task> currentTaskList = new ArrayList<>();

    public void createTask(String projectId, String id, String name, String description, Date dateStart, Date dateFinish) {
        taskList.add(new Task(projectId, id, name, description, dateStart, dateFinish));
    }

    public void deleteTask(String id) {
        for (Task task : taskList) {
            if (task.getId().equals(id)) {
                taskList.remove(task);
                break;
            }
        }
    }

    public void updateTask(String id, String name, String description, Date dateStart, Date dateFinish) {
        for (Task task : taskList) {
            if (task.getId().equals(id)) {
                task.setName(name);
                task.setDescription(description);
                task.setDateStart(dateStart);
                task.setDateFinish(dateFinish);
                break;
            }
        }
    }

    public void deleteAllTaskByProjectId(String projectId) {
        for (Task task : taskList) {
            if (task.getProjectId().equals(projectId)) {
                currentTaskList.add(task);
            }
        }
        taskList.removeAll(currentTaskList);
        currentTaskList.removeAll(currentTaskList);
    }

    public void clearTaskList() {
        taskList.removeAll(taskList);
    }

    public List<Task> showTaskList() {
        return taskList;
    }
}
