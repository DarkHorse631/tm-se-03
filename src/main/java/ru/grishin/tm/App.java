package ru.grishin.tm;

import ru.grishin.tm.ui.Command;
import ru.grishin.tm.ui.Menu;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Menu menu = new Menu();
        Scanner scanner = new Scanner(System.in);
        String input;
        System.out.println("[***WELCOME TO TASK MANAGER***]");
        do {
            System.out.print("--Input:");
            input = scanner.nextLine();
            Command command = Command.checkCommand(input);
            if (command.equals(null)) {
                System.out.println("---!!!Wrong command!!!---");
            }
            switch (command) {
                case PROJECT_CREATE:
                    menu.createProject();
                    break;
                case PROJECT_SHOW_ALL:
                    menu.showProjectList();
                    break;
                case PROJECT_DELETE:
                    menu.deleteProject();
                    break;
                case PROJECT_UPDATE:
                    menu.updateProject();
                    break;
                case PROJECT_CLEAR:
                    menu.clearProject();
                    break;
                case TASK_CREATE:
                    menu.createTask();
                    break;
                case TASK_UPDATE:
                    menu.updateTask();
                    break;
                case TASK_SHOW_ALL:
                    menu.showTaskList();
                    break;
                case TASK_DELETE:
                    menu.deleteTask();
                    break;
                case HELP:
                    menu.showHelpList();
                    break;
                case EXIT:
                    menu.exit();
                    break;
            }
        } while (true);
    }
}