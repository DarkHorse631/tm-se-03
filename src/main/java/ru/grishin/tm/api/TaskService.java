package ru.grishin.tm.api;

import ru.grishin.tm.entity.Task;

import java.util.Date;
import java.util.List;

public interface TaskService {

    void createTask(String projectId, String taskName, String description, Date startDate, Date completionDate);

    void deleteTask(String taskId);

    void updateTask(String taskId, String taskName, String description, Date startDate, Date completionDate);

    Task readTask(String projectId);

    List<Task> showAll();
}
