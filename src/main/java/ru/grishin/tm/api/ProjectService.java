package ru.grishin.tm.api;

import ru.grishin.tm.entity.Project;

import java.util.Date;

public interface ProjectService {

    void createProject(String projectName, String description, Date startDate, Date completionDate);

    void deleteProject(String projectId);

    void update(String projectId, String projectName, String description, Date startDate, Date completionDate);

    Project readProject(String projectId);
}
